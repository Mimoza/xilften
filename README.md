# Xilften

Test d'implémentation de la pile Netflix avec Spring boot.
Les tuto d'exemple suivits sont listé ci dessous. ils ont été suivi dans l'ordre.

## Zuul
Celui qui m'a servit de base
* https://spring.io/guides/gs/routing-and-filtering/

Celui qui m'a confirmer un certain nombre de point (surtout le error filter)
* http://javasampleapproach.com/spring-framework/spring-cloud/configure-springboot-zuul-routing-filtering

J'ai fait une monté de version de Spring-boot pour me mettre à jour la dépendance à Zuul et ainsi faire fonctionner le filtre «error».

## Ribbon
Ribbon permet la repartition de charge sur différentes instance du même service
Celui de base, mais non suivi car ne prend pas en compte Zull
* https://spring.io/guides/gs/client-side-load-balancing/
Celui qui m'a aidé :
https://blog.heroku.com/using_netflix_zuul_to_proxy_your_microservices

## Hysterix
Tuto de base :
* https://spring.io/guides/gs/circuit-breaker/

La doc officiel qui donne un exemple de code
* https://github.com/spring-cloud/spring-cloud-netflix/blob/master/docs/src/main/asciidoc/spring-cloud-netflix.adoc#providing-hystrix-fallbacks-for-routes

Ne pas oublier la conf :
* https://github.com/spring-cloud/spring-cloud-netflix/blob/master/docs/src/main/asciidoc/spring-cloud-netflix.adoc#uploading-files-through-zuul
 
## Eureka
Eureka permet le référencement dynamique de nouveau service
Tuto de base
* https://spring.io/guides/gs/service-registration-and-discovery/

Celui qui m'a aidé a finir :
* http://ippon.developpez.com/tutoriels/spring/microservices-netflixoss/
