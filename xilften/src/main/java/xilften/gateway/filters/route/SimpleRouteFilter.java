package xilften.gateway.filters.route;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.Route;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Component
public class SimpleRouteFilter extends ZuulFilter {

	private RouteLocator routeLocator;

	@Autowired
	SimpleRouteFilter(RouteLocator routeLocator){
		this.routeLocator = routeLocator;
	}

	@Override
	public String filterType() {
		return FilterConstants.ROUTE_TYPE;
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		String path = request.getServletPath();
		Route route = routeLocator.getMatchingRoute(path);
		String target = ctx.getRouteHost() + route.getPath();
		log.info(String.format("%s is routing to %s", request.getRequestURL().toString(), target));

		return null;
	}
}