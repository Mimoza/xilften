package xilften.gateway.filters.post;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class SimplePostFilter extends ZuulFilter {

	@Override
	public String filterType() {
		return FilterConstants.POST_TYPE;
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		HttpServletResponse response = ctx.getResponse();

		log.info(String.format("Response's content type is %s", response.getStatus()));
		log.info("REQUEST :: < " + request.getScheme() + ' ' + request.getLocalAddr() + ':' + request.getLocalPort());
		log.info("REQUEST :: < " + request.getMethod() + ' ' + request.getRequestURI() + ' ' + request.getProtocol());
		log.info("RESPONSE:: > HTTP:" + response.getStatus());

		return null;
	}
}