package xilften.gateway.filters.error;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;

/**
 * Ne semble pas foncionner malgré le fait que toute la doc qui le dit :
 * https://github.com/Netflix/zuul/wiki/How-it-Works
 * https://spring.io/guides/gs/routing-and-filtering/
 *
 * Peut être que c'est la version de Zuul de spring boot qui ne le permet pas.
 * Voir l'exemple de «SendErrorFilter» qui est passé de «post» à «error» dans ce commit (à partir de la version 1.3.0.M1 de spring-cloud-netflix)
 * https://github.com/spring-cloud/spring-cloud-netflix/commit/08fe5a89e9b6e13c72b0ec6e4c57ad11c26e1c07
 *
 * Après monté de version de Springboot en 1.5.7 + springcloud en Dalston.SR3 Ça fonctionne enfin !!!
 * Maintenant il existe un fichier de constante pour la méthode «filterType»
 * https://github.com/spring-cloud/spring-cloud-netflix/blob/master/spring-cloud-netflix-core/src/main/java/org/springframework/cloud/netflix/zuul/filters/support/FilterConstants.java
 **/


@Slf4j
@Component
public class SimpleErrorFilter extends ZuulFilter {

	@Override
	public String filterType() {
		return FilterConstants.ERROR_TYPE;
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletResponse response = ctx.getResponse();

		log.error(String.format("response status is %d", response.getStatus()));

		return null;
	}
}