package services.book;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@Slf4j
@RestController
@EnableDiscoveryClient
@SpringBootApplication
public class BookApplication {

	public static void main(String... args) {
		setProperties();
		SpringApplication.run(BookApplication.class, args);
	}

	private static void setProperties() {
		System.setProperty("spring.application.name", "books");
		System.setProperty("server.port", Integer.toString(getRandom(9000, 10000)));
	}

	private static int getRandom(int min, int max) {
		Random rand = new Random();
		return rand.nextInt(max - min + 1) + min;
	}

	@RequestMapping("/available")
	public String available() {
		log.info("Disponibilté de livre");
		return "Book available";
	}

	@RequestMapping("/checked-out")
	public String checkedOut() {
		log.info("Retrait de livre");
		return "Checked-out book";
	}

	@ResponseStatus(value= HttpStatus.I_AM_A_TEAPOT, reason = "Time to take a tea")
	@RequestMapping("/tea")
	public String tea() {
		log.info("Tea time !");
		return "Book tea time";
	}

	@RequestMapping("/")
	public String home() {
		log.info("Home livre");
		return "Service de livre";
	}
}