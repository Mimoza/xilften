package services.hello;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Slf4j
@RestController
@EnableDiscoveryClient
@SpringBootApplication
public class SayHelloApplication {

	public static void main(String... args) {
		setProperties();
		SpringApplication.run(SayHelloApplication.class, args);
	}

	private static void setProperties() {
		System.setProperty("spring.application.name", "hello");
		System.setProperty("server.port", Integer.toString(getRandom(9000, 10000)));
	}

	private static int getRandom(int min, int max) {
		Random rand = new Random();
		return rand.nextInt(max - min + 1) + min;
	}

	@RequestMapping("/greeting")
	public String greet() {
		log.info("Access /greeting");

		List<String> greetings = Arrays.asList("Hi there", "Greetings", "Salutations");
		Random rand = new Random();

		int randomNum = rand.nextInt(greetings.size());
		return greetings.get(randomNum);
	}

	@RequestMapping("/")
	public String home() {
		log.info("Access /");
		return "Hi!";
	}
}
